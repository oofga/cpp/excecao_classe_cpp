#include <iostream>
#include <exception>

#include "excecao.hpp"

using namespace std;

const Excecao e_ValorForaDosLimites("O valor está fora dos limites.");
const Excecao e_Overflow("Oveflow de dados");
const Excecao e_EntradaInvalida("Entrada de teclado inválida");

void funcao_com_erro(int tipo) {
   switch (tipo) {
	case 1:
   	   throw Excecao("Erro: Função com Problema");
	   break;
	case 2:
	   throw exception();
 	   break;
	case 3:
	   throw e_ValorForaDosLimites;
	   break;
	default:
	   break;
	}
}

int main(int argc, char ** argv) {
   try {
	funcao_com_erro(3);
   }
   catch(Excecao & e){
	cout << "Exceção capturada: " << e.what() << endl;
   }
   catch(std::exception &e){
         cout << "Exception: " << e.what() << endl;
    }


   return 0;
}
